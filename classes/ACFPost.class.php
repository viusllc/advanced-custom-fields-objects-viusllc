<?php

if( !class_exists('ACFPost') ) {
	
	class ACFPost {
		
		/**
		 * @var WP_Post The WP_Post object for this object.
		 */
		protected $_post;
		
		/**
		 * @var multitype:multitype Holds the acf_field objects for this post.
		 */
		protected $_acfFieldObjects = array();
		
		/**
		 * @param ambiguous <WP_Post|string|integer> $post The WP_Post object for this object, or a string/integer representation of the Post ID to load for it.
		 */
		public function __construct( $post ) {
			
			//if the post is not a WP_Post object and it converts to an integer, load the post assuming this is an ID
			if( !($post instanceof WP_Post) && (integer)$post ) $post = get_post( $post );
			
			$this->_post = $post;
			$this->_getFieldObjects();
			
		}
		
		/**
		 * This function loads all ACF field objects (and their values) into memory for the given post.
		 */
		protected function _getFieldObjects() {
			
			$this->_acfFieldObjects = get_field_objects( $this->_post->ID );
			
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @param ambiguous $value The value to set for that field.
		 */
		public function setField( $field_name , $value ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return update_field( $this->_acfFieldObjects[ $field_name ]['key'] , $value , $this->_post->ID );
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return string The ACF label for this field.
		 */
		public function getFieldLabel( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['label'];
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return string The ACF instructions for this field.
		 */
		public function getFieldInstructions( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['instructions'];
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return string The ACF type of this field.
		 */
		public function getFieldType( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['type'];
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return string The ACF key for this field.
		 */
		public function getFieldKey( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['key'];
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return string The ACF-generated ID attribute for this field.
		 */
		public function getFieldId( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['id'];
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return ambiguous <multitype:multitype|boolean> An array of the ACF sub-fields, or false if there are none.
		 */
		public function getSubFields( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) || !isset( $this->_acfFieldObjects[ $field_name ]['sub_fields'] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['sub_fields'];
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return boolean True if the field is required, false otherwise.
		 */
		public function isFieldRequired( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['required'] ? true : false;
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return string 
		 */
		public function filterContent( $field_name ) {
			//alias the value locally so we don't write to the object directly
			$val = $this->$field_name;
			//if the value is not a string, return it without filtering
			if( !is_string($val) ) return $val;
			//filter the value
			$val = apply_filters( 'the_content' , $val );
			//return it
			return $val;
		}
		
		/*
		 * Magic stuff
		 */
		
		public function __get( $key ) {
			
			//special case keys
			switch( $key ) {
				case 'post_date':
				case 'post_date_gmt':
				case 'post_modified':
				case 'post_modified_gmt':
					$timezoneString = get_option('timezone_string');
					if( $timezoneString ) {
						return new DateTime( $this->_post->$key , new DateTimeZone($timezoneString) );
					} else {
						return new DateTime( $this->_post->$key );
					}
				case 'post':
					return $this->_post;
			}
			
			
			//check if this is a special case field we're working on
			switch( $this->_acfFieldObjects[ $key ]['type'] ) {
				case 'date_picker':
					//create a date object based on the db storage format used for this field
					$timezoneString = get_option('timezone_string');
					$dbFormat = str_replace( array( 'yy' , 'mm' , 'dd' ) , array( 'Y' , 'm' , 'd' ) , $this->_acfFieldObjects[ $key ]['date_format'] );
					if( $timezoneString ) {
						return DateTime::createFromFormat( $dbFormat , $this->_acfFieldObjects[ $key ]['value'] , new DateTimeZone( $timezoneString ) );
					} else {
						return DateTime::createFromFormat( $dbFormat , $this->_acfFieldObjects[ $key ]['value'] );
					}
			}
			
			
			//get the value from acf fields if it exists there
			if( isset( $this->_acfFieldObjects[ $key ]['value'] ) ) return $this->_acfFieldObjects[ $key ]['value'];
			
			
			//get the value from wp_post if it exists there
			if( $this->_post->$key ) return $this->_post->$key;
			
			
			//return nothing if there's no match
			return;
		}
		
		public function __isset( $key ) {
			//post is always set
			if( $key == 'post' ) return true;
			//if the field exists on the acf data
			if( isset( $this->_acfFieldObjects->$key ) ) return true;
			//if the field exists on the wp_post
			if( isset( $this->_post->$key ) ) return true;
			return false;
		}
		
	}
	
}