<?php

if( !class_exists('ACFTerm') ) {
	
	class ACFTerm {
		
		/**
		 * @var WP Term The WP Term object for this object.
		 */
		protected $_term;
		
		/**
		 * @var multitype:multitype Holds the acf_field objects for this term.
		 */
		protected $_acfFieldObjects = array();
		
		/**
		 * @param object $term The WP Term database row object containing the data for this term.
		 */
		public function __construct( $term ) {
			
			$this->_term = $term;
			$this->_getFieldObjects();
			
		}
		
		/**
		 * This function loads all ACF field objects (and their values) into memory for the given term.
		 */
		protected function _getFieldObjects() {
			
			$this->_acfFieldObjects = get_field_objects( $this->_term );
			
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @param ambiguous $value The value to set for that field.
		 */
		public function setField( $field_name , $value ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return update_field( $this->_acfFieldObjects[ $field_name ]['key'] , $value , $this->_term );
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return string The ACF label for this field.
		 */
		public function getFieldLabel( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['label'];
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return string The ACF instructions for this field.
		 */
		public function getFieldInstructions( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['instructions'];
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return string The ACF type of this field.
		 */
		public function getFieldType( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['type'];
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return string The ACF key for this field.
		 */
		public function getFieldKey( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['key'];
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return string The ACF-generated ID attribute for this field.
		 */
		public function getFieldId( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['id'];
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return ambiguous <multitype:multitype|boolean> An array of the ACF sub-fields, or false if there are none.
		 */
		public function getSubFields( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) || !isset( $this->_acfFieldObjects[ $field_name ]['sub_fields'] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['sub_fields'];
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return boolean True if the field is required, false otherwise.
		 */
		public function isFieldRequired( $field_name ) {
			//if we don't recognize this field, return false
			if( !isset( $this->_acfFieldObjects[ $field_name ] ) ) return false;
			return $this->_acfFieldObjects[ $field_name ]['required'] ? true : false;
		}
		
		/**
		 * @param string $field_name The name of the field (DO NOT use the ACF key here, use the human readable name).
		 * @return string 
		 */
		public function filterContent( $field_name ) {
			//alias the value locally so we don't write to the object directly
			$val = $this->$field_name;
			//if the value is not a string, return it without filtering
			if( !is_string($val) ) return $val;
			//filter the value
			$val = apply_filters( 'the_content' , $val );
			//return it
			return $val;
		}
		
		/*
		 * Magic stuff
		 */
		
		public function __get( $key ) {
			
			//special case keys
			switch( $key ) {
				case 'term':
					return $this->_term;
			}
			
			
			//check if this is a special case field we're working on
			switch( $this->_acfFieldObjects[ $key ]['type'] ) {
				case 'date_picker':
					//create a date object based on the db storage format used for this field
					$timezoneString = get_option('timezone_string');
					$dbFormat = str_replace( array( 'yy' , 'mm' , 'dd' ) , array( 'Y' , 'm' , 'd' ) , $this->_acfFieldObjects[ $key ]['date_format'] );
					if( $timezoneString ) {
						return DateTime::createFromFormat( $dbFormat , $this->_acfFieldObjects[ $key ]['value'] , new DateTimeZone( $timezoneString ) );
					} else {
						return DateTime::createFromFormat( $dbFormat , $this->_acfFieldObjects[ $key ]['value'] );
					}
			}
			
			
			//get the value from acf fields if it exists there
			if( isset( $this->_acfFieldObjects[ $key ]['value'] ) ) return $this->_acfFieldObjects[ $key ]['value'];
			
			
			//get the value from WP Term if it exists there
			if( $this->_term->$key ) return $this->_term->$key;
			
			
			//return nothing if there's no match
			return;
		}
		
		public function __isset( $key ) {
			//term is always set
			if( $key == 'term' ) return true;
			//if the field exists on the acf data
			if( isset( $this->_acfFieldObjects->$key ) ) return true;
			//if the field exists on the WP Term
			if( isset( $this->_term->$key ) ) return true;
			return false;
		}
		
	}
	
}