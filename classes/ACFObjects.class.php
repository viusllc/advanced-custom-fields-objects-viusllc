<?php


if( !class_exists('ACFObjects') ) {
	
	class ACFObjects {
		
		const DIR_ROOT = ACFOBJECTS_DIR_ROOT;
		
		public static function on_activate() {
			
			if( !self::_checkPrereqs() ) {
				//prereqs not met
				trigger_error( 'The "advanced-custom-fields" plugin must be installed and active in order to use the ACF Objects plugin.' , E_USER_ERROR );
				return;
			}
			
		}
		
		public static function on_init() {
			
			if( !self::_checkPrereqs() ) {
				//prereqs not met
				trigger_error( 'The "advanced-custom-fields" plugin must be installed and active in order to use the ACF Objects plugin. The functions and classes will not be available unless the ACF plugin is installed and active.' , E_USER_WARNING );
				return;
			}
			
			//if we're here, we meet the prereqs
			require_once self::DIR_ROOT . 'classes/ACFPost.class.php';
			require_once self::DIR_ROOT . 'classes/ACFTerm.class.php';
			
			
			//pull in the helper functions
			require_once self::DIR_ROOT . 'functions/advanced-custom-fields-objects-functions.php';
			
		}
		
		protected static function _checkPrereqs() {
			
			// if( !function_exists( 'is_plugin_active' ) ) {
			// 	require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			// }
			
			// return is_plugin_active('advanced-custom-fields/acf.php');
			
			return class_exists('acf');
			
		}
		
	}
	
}